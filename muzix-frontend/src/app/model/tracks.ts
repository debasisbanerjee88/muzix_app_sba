export class Tracks{
    id:string;
    userId:string;
    name:string;
    type:string;
    artistName:string;
    description:string;
    previewURL:string;
    imageURL:string;
    albumId:string;
    isFavourite:boolean;
   
}