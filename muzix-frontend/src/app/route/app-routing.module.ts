import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
//import { PageNotFoundComponent } from '../components/page-not-found/page-not-found.component';

//import { AuthGuard } from '../authguard/auth.guard';
//import { HeaderComponent } from '../components/header/header.component';
import { FavouriteComponent } from '../components/favourite/favourite.component';
import { SearchComponent } from '../components/search/search.component';
import { DashboardComponent } from '../components/dashboard/dashboard.component';
import { PagenotfoundComponent } from '../components/pagenotfound/pagenotfound.component';
import { PlaylistComponent } from '../components/playlist/playlist.component';
import { MusicsComponent } from '../components/musics/musics.component';
import { RecommendedComponent } from '../components/recommended/recommended.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  // {
  //   path: 'register',
  //   component: RegisterComponent
  // },
  // {
  //   path: 'login',
  //   component: LoginComponent
  // },
  {
    path: 'home',
    component: DashboardComponent,
    //canActivate:[AuthGuard]
  },
  // {
  //   path: 'moviedetails/:id',             // Add /:id here
  //   component: MoviedetailsComponent,
  //   canActivate:[AuthGuard]
  // },
  //
  {
    path: 'search',             // Add /:id here
    component: SearchComponent//,
    //canActivate:[AuthGuard]
  },
  {
    path: 'favourite',
    component: FavouriteComponent
    //canActivate:[AuthGuard]
  },
  {
    path: '**',
    component: PagenotfoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [
  DashboardComponent,
  FavouriteComponent,
  SearchComponent,
  PlaylistComponent,
  MusicsComponent,
  RecommendedComponent,
  PagenotfoundComponent
  ]