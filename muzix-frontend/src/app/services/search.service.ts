import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Tracks } from '../model/tracks';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(private _http:HttpClient) { }
  
  getSearchItems(searchText)
  {
    let _searchURL=`http://api.napster.com/v2.2/search?apikey=YTkxZTRhNzAtODdlNy00ZjMzLTg0MWItOTc0NmZmNjU4Yzk4&query=${searchText}&type=playlist,track`;
    return this._http.get<any>(_searchURL);
  }

  makeFavourite(item:any)
  {
    item.userId='banedeb';
    let body=JSON.stringify(item);
    let makeFavouriteURL=`${environment.BASE_URL}${environment._ADD_TO_FAVOURITE}`;
    return this._http.post(makeFavouriteURL,body,httpOptions);
  }

  removeFavourite(trackId)
  {
    let userId='banedeb';
    let removeFavURL=`${environment.BASE_URL}${environment._DELETE_FAV_NEWS}${trackId}/${userId}`;
    
    return this._http.delete(removeFavURL)
  }

  getFavourite():Observable<Tracks[]>
  {
    let getFavouriteURL=`${environment.BASE_URL}${environment._GET_FAVOURITE_NEWS}banedeb`;
    return this._http.get<Tracks[]>(getFavouriteURL);
  }
}
