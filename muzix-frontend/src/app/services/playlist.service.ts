import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpHeaders } from '@angular/common/http';
import { Tracks } from '../model/tracks';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class PlaylistService {
  
  constructor(private _http:HttpClient) { }
  
  getPlayList():Observable<any>
  {
    return this._http.get<any>('http://api.napster.com/v2.2/playlists?apikey=YTkxZTRhNzAtODdlNy00ZjMzLTg0MWItOTc0NmZmNjU4Yzk4');
  }

  makeFavourite(playlist:any)
  {
    playlist.userId='banedeb';
    let body=JSON.stringify(playlist);
    let makeFavouriteURL=`${environment.BASE_URL}${environment._ADD_TO_FAVOURITE}`;
    return this._http.post(makeFavouriteURL,body,httpOptions);
  }

  removeFavourite(trackId)
  {
    let userId='banedeb';
    let removeFavURL=`${environment.BASE_URL}${environment._DELETE_FAV_NEWS}${trackId}/${userId}`;
    
    return this._http.delete(removeFavURL)
  }

  getFavourite():Observable<Tracks[]>
  {
    let getFavouriteURL=`${environment.BASE_URL}${environment._GET_FAVOURITE_NEWS}banedeb`;
    return this._http.get<Tracks[]>(getFavouriteURL);
  }
}
