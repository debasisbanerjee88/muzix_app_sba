import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Tracks } from '../model/tracks';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class FavouriteService {

  constructor(private _http:HttpClient) { }

  getFavourite():Observable<Tracks[]>
  {
   
    let getFavouriteURL=`${environment.BASE_URL}${environment._GET_FAVOURITE_NEWS}banedeb`;
    return this._http.get<Tracks[]>(getFavouriteURL);
  }

  removeFavourite(trackId)
  {
    let userId='banedeb';
    let removeFavURL=`${environment.BASE_URL}${environment._DELETE_FAV_NEWS}${trackId}/${userId}`;
    
    return this._http.delete(removeFavURL)
  }
}
