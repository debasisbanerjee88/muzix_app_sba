import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  searchText:string;

  constructor(private _router: Router) { }

  ngOnInit() {

  }
  getSearchItems(searchText)
  {
   if(!!searchText)
    this._router.navigate(['/search',{q:searchText}]);
  }

}
