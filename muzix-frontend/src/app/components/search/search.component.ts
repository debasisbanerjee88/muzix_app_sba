import { Component, OnInit } from '@angular/core';
import { SearchService } from 'src/app/services/search.service';
import { ActivatedRoute } from '@angular/router';
import { Tracks } from 'src/app/model/tracks';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  searchText:string;
  tracks:any;
  playlists:any;
  favourites:Tracks[];
  constructor(
    private _search: SearchService,
    private activeRoute: ActivatedRoute) { 
      this.activeRoute.params.subscribe(params => {
        this.searchText = params['q']; 
        if(!!this.searchText)
        {
          this.getSearchItems(this.searchText)
        }
      });
    }

  ngOnInit() {
 
  }

   
  private getSearchItems(searchText)
  {
    this._search.getSearchItems(searchText).subscribe(data=>
    {
      this.tracks=data.search.data.tracks;
      for(let track of this.tracks)
      track.imageURL=`http://direct.rhapsody.com/imageserver/v2/albums/${track.albumId}/images/300x300.jpg`
      this.playlists=data.search.data.playlists;
      this.getFavourite('dashboard');
     },
     err=>{
       //console.error(err);
       alert(err.error);
     });
  }

  public toggleFavourite(item:any)
  {
    if(item.type=='playlist')
    item.imageURL=item.images[0].url;
    if(!item.isFavourite)
    {
      this._search.makeFavourite(item).subscribe(response=>{
       item.isFavourite=true;
      },
      err=>{
        console.log(err.error);
        }); 
    }
    else{
      this._search.removeFavourite(item.id).subscribe(response=>{
        item.isFavourite=false;
       },
       err=>{
         console.log(err.error);
         }); 
    }
  
  }

  public getFavourite(from:string)
  {
    this._search.getFavourite().subscribe(response=>{
      this.favourites=response;
     
      for(let track of this.tracks)
       {
        for(let ftrack of this.favourites)
        {
          if(track.id==ftrack.id)
          {
            track.isFavourite=true;
          }
        }
       }
     
       for(let plist of this.playlists)
       {
        for(let ftrack of this.favourites)
        {
          if(plist.id==ftrack.id)
          {
            plist.isFavourite=true;
          }
        }
       }
    
    },
    err=>{
      console.log(err.error);
      }); 
  }
}
