﻿using auth_server.CustomException;
using auth_server.Data.Model;
using auth_server.MessageConstants;
using auth_server.Service;
using Microsoft.AspNetCore.Mvc;
using System;

namespace auth_server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        public readonly IUserService _userService;
        public readonly ITokenGeneratorService _tokenGeneratorService;

        public AuthController(IUserService userService, ITokenGeneratorService tokenGeneratorService)
        {
            _userService = userService;
            _tokenGeneratorService = tokenGeneratorService;
        }

        [HttpPost("register")]
        public IActionResult Register([FromBody] User user)
        {
            try
            {
                bool isUserExsist = _userService.IsUserExsist(user.UserId);
                if (isUserExsist)
                {
                    return Conflict(StatusMessage.UserExsistStatus409);
                }
                else {
                    _userService.Register(user);
                    return Ok(new { data = StatusMessage.RegisterSuccessStatus201 });
                }
            }
            catch (Exception)
            {
                return BadRequest(StatusMessage.InternalServerErrorStatus500);
            }
        }


        [HttpPost("login")]
        public IActionResult Login([FromBody] User user)
        {
            try
            {
                string userId = user.UserId;
                string password = user.Password;
                User currentUser = _userService.Login(userId, password);

                //get Jwt token for respective user
                string token = _tokenGeneratorService.GetJwtTokenString(userId);
                //return tokken with response
                return Ok(token);
            }
            catch (UserNotFoundException notfound)
            {
                return NotFound(notfound.Message);
            }
            catch (Exception)
            {
                return BadRequest(StatusMessage.InternalServerErrorStatus500);
            }
        }

    }
}