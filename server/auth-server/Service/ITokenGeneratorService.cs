﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace auth_server.Service
{
    public interface ITokenGeneratorService
    {
       string  GetJwtTokenString(string userId);
    }
}
