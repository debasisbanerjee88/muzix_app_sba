﻿using auth_server.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace auth_server.Service
{
    public interface IUserService
    {
        /// <summary>
        /// Check if requsted userId exsit or not
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        bool IsUserExsist(string userId);

        /// <summary>
        /// Create a user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        User Register(User user);

        /// <summary>
        /// Get user if valid
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        User Login(string userId, string password);

    }
}
