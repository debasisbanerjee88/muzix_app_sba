﻿using auth_server.CustomException;
using auth_server.Data.Model;
using auth_server.Data.Persistence;
using auth_server.MessageConstants;

namespace auth_server.Service
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _repo;
        public UserService(IUserRepository repo)
        {
            _repo = repo;
        }
        /// <summary>
        /// Check if requsted userId exsit or not
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool IsUserExsist(string userId)
        {
            var user = _repo.FindUserById(userId);
            if (user != null)
            {
                return true;
            }
            else {
                return false;
            }
        }

        /// <summary>
        /// Get user if valid
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public User Login(string userId, string password)
        {
            var user = _repo.Login(userId, password);
            if (user != null)
            {
                return user;
            }
            else {
                throw new UserNotFoundException(ExceptionMessage.UserNotFound);
            }
        }

        /// <summary>
        /// Create a user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public User Register(User user)
        {
           return _repo.Register(user);
        }
    }
}
