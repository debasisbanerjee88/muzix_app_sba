﻿using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace auth_server.Service
{
    public class TokenGeneratorService : ITokenGeneratorService
    {
        public string GetJwtTokenString(string userId)
        {
            //setp: 1
            //setting the claims on UserId
            var userclaims = new[]
            {
                new Claim(JwtRegisteredClaimNames.UniqueName,userId),
                new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString())
            };

            //setp: 2
            //Defining the security key and encoding the claim /signiture setup
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("gfhfhfhfhfhgfgsgf7676566dfdtd6e5d5drxr4s4w3"));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            //setp:3
            //seting the Jwt token information and expiration time
            var token = new JwtSecurityToken(
                issuer: "AuthServer",
                audience: "newsapp",
                claims: userclaims,
                expires: DateTime.UtcNow.AddMinutes(10),
                signingCredentials: creds
                );

            //setp:4
            //seting up the response of token
            var response = new
            {
                token = new JwtSecurityTokenHandler().WriteToken(token)
            };

            //step 5:
            //converting into Json by serializing token response

            return JsonConvert.SerializeObject(response);
        }
    }
}
   
