﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using auth_server.Data.Model;

namespace auth_server.Data.Persistence
{
    public class UserRepository : IUserRepository
    {
        private readonly IUserDbContext _dbcontext;
        public UserRepository(UserDbContext dbcontext)
        {
            _dbcontext = dbcontext;
        }
        /// <summary>
        /// Get user by Id
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public User FindUserById(string userId)
        {
            return _dbcontext.Users.FirstOrDefault(u => u.UserId == userId);
        }

        /// <summary>
        /// Get user if valid
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public User Login(string userId, string password)
        {
            return _dbcontext.Users.FirstOrDefault(u =>(u.UserId == userId && u.Password==password));
        }

        /// <summary>
        /// Create a user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public User Register(User user)
        {
            _dbcontext.Users.Add(user);
            _dbcontext.SaveChanges();
            return user;
        }
    }
}
