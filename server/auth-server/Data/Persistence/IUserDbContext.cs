﻿using auth_server.Data.Model;
using Microsoft.EntityFrameworkCore;

namespace auth_server.Data.Persistence
{
    public interface IUserDbContext
    {
        DbSet<User> Users { get; set; }
        int SaveChanges();
    }
}
