﻿using auth_server.Data.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace auth_server.Data.Persistence
{
    public class UserDbContext :DbContext,IUserDbContext
    {
        public UserDbContext()
        {
        }

        public UserDbContext(DbContextOptions Options) : base(Options)
        {
            this.Database.EnsureCreated();
        }

        public DbSet<User> Users { get; set; }
    }
}
