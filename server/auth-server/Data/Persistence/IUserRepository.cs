﻿using auth_server.Data.Model;

namespace auth_server.Data.Persistence
{
    public interface IUserRepository
    {
        /// <summary>
        /// Create a user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        User Register(User user);

        /// <summary>
        /// Get user if valid
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        User Login(string userId, string password);

        /// <summary>
        /// Get user by Id
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        User FindUserById(string userId);
    }
}
