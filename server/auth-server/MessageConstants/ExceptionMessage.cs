﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace auth_server.MessageConstants
{
    public static class ExceptionMessage
    {
        public const string UserNotFound = "Requested user not found!";
    }
}
