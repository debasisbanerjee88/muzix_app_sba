﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace auth_server.MessageConstants
{
    public static class StatusMessage
    {
        public const string UserExsistStatus409 = "User already exsist!";
        public const string RegisterSuccessStatus201 = "You are successfully registered!";
        public const string InternalServerErrorStatus500 = "Internal server error occured!";
        
    }
}
