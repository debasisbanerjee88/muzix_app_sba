﻿using ExpectedObjects;
using Microsoft.AspNetCore.Mvc;
using Moq;
using server.Controllers;
using server.CustomException;
using server.Data.Model;
using server.MessageConstants;
using server.Service;
using System;
using System.Collections.Generic;
using System.Net;
using Xunit;

namespace test
{
    public class NewsControllerTest
    {
        

        private List<News> GetAllNews()
        {
            List<News> News = new List<News>();
            News.Add(new News { NewsId = 1122, Name = "Test1",Title="Title1", Content = "Comment 1", UrlToImage = "PosterPath1.jpg", PublishedAt = "01/02/2019", Url = "timesofindia.in", UserId="586535" });
            News.Add(new News { NewsId = 1123, Name = "Test2",Title="Title2", Content = "Comment 2", UrlToImage = "PosterPath2.jpg", PublishedAt = "02/02/2019", Url = "timesofindia.in", UserId="586535" });
            News.Add(new News { NewsId = 1124, Name = "Test3",Title="Title3", Content = "Comment 3", UrlToImage = "PosterPath3.jpg", PublishedAt = "03/02/2019", Url = "timesofindia.in", UserId="586535" });
            News.Add(new News { NewsId = 1125, Name = "Test4",Title="Title4", Content = "Comment 4", UrlToImage = "PosterPath4.jpg", PublishedAt = "04/02/2019", Url = "timesofindia.in", UserId ="586535" });
            return News;
        }

        #region GetAllNews
        /// <summary>
        /// Test to return all News 
        /// </summary>
        [Fact]
        public void GetAllNews_ShouldReturnAllNews()
        {
            //Arrange
            var MockService = new Mock<INewsCruiserService>();
            MockService.Setup(serv => serv.GetAllNews("586535")).Returns(this.GetAllNews());
            var _controller = new NewsController(MockService.Object);

            // Act
            var okResult = _controller.GetAllNews("586535");

            // Assert
            var actionResult = Assert.IsType<OkObjectResult>(okResult);
            var model = Assert.IsAssignableFrom<List<News>>(actionResult.Value);
            Assert.Equal(4, model.Count);
        }
        #endregion

        #region GetNewsById
        [Fact]
        public void GetNewsById_ShouldReturnANewsMatchedWithId()
        {
            //Arrange
            var expectedReturn = new News
            {
                NewsId = 1122,
                Name = "Test1",
                Content = "Comment 1",
                UrlToImage = "PosterPath1.jpg",
                PublishedAt = "01/02/2019",
                Url = "timesofindia.in",
                UserId = "586535"
            };
           
            var MockService = new Mock<INewsCruiserService>();
            MockService.Setup(serv => serv.GetNewsById(1122,"586535")).Returns(expectedReturn);
            var _controller = new NewsController(MockService.Object);


            //Act
            var acctual = _controller.GetNewsById(1122,"586535");

            //Assert
            var model= Assert.IsType<OkObjectResult>(acctual);
            Assert.Equal(HttpStatusCode.OK, (HttpStatusCode)model.StatusCode);
            expectedReturn.ToExpectedObject().ShouldEqual(model.Value);
        }

        [Fact]
        public void GetNewsById_ShouldReturnNewsNotFound()
        {
            //Arrange
            var MockService = new Mock<INewsCruiserService>();
            MockService.Setup(serv => serv.GetNewsById(1129,"nouser")).Throws(new NewsNotFoundException("No such News found!"));
            var _controller = new NewsController(MockService.Object);

            //Act
            var acctual = _controller.GetNewsById(1129,"nouser");

            //Assert
            var model = Assert.IsType<NotFoundObjectResult>(acctual);

            Assert.Equal(HttpStatusCode.NotFound, (HttpStatusCode)model.StatusCode);
            
            Assert.Equal("No such News found!", model.Value);

        }

        [Fact]
        public void GetNewsById_ShouldReturnANewsIdWithZero()
        {
            //Arrange
            var MockService = new Mock<INewsCruiserService>();
            MockService.Setup(serv => serv.GetNewsById(It.IsAny<int>(), It.IsAny<string>())).Throws(new NewsNotFoundException(ApplicationMessages.NewsNotFoundMsg));
            var _controller = new NewsController(MockService.Object);

            //Act
            var acctual = _controller.GetNewsById(0,"");

            //Assert
            var model = Assert.IsType<NotFoundObjectResult>(acctual);
            Assert.Equal(HttpStatusCode.NotFound, (HttpStatusCode)model.StatusCode);
            Assert.Equal(ApplicationMessages.NewsNotFoundMsg, model.Value);
        }
        #endregion

        #region AddNewsToDatabase
        [Fact]
        public void AddNewsToDatabase_ShouldReturnSuccessMessage()
        {
            //Arrange
            News postedObject = new News
            {
                Name = "Test1",
                Title = "Test1",
                Content = "Comment 1",
                UrlToImage = "PosterPath1.jpg",
                PublishedAt = "01/02/2019",
                Url = "timesofindia.in",
                UserId = "586535"
            };
            var MockService = new Mock<INewsCruiserService>();
            MockService.Setup(serv => serv.AddNewsToDatabase(It.IsAny<News>())).Returns("Success");
            var _controller = new NewsController(MockService.Object);

            //Act
            var acctual = _controller.AddNewsToDatabase(postedObject);

            //Assert
            var model = Assert.IsType<OkResult>(acctual);
            Assert.Equal(HttpStatusCode.OK, (HttpStatusCode)model.StatusCode);
            
            
        }

        [Fact]
        public void AddNewsToDatabase_ShouldReturnExceptionMessage_IfNews_Exist_InDatabase()
        {
            //Arrange
            News postedObject = new News
            {
                Name = "Test1",
                Title= "Test1",
                Content = "Comment 1",
                UrlToImage = "PosterPath1.jpg",
                PublishedAt = "01/02/2019",
                Url = "timesofindia.in",
                UserId = "586535"
            };

            var MockService = new Mock<INewsCruiserService>();
            MockService.Setup(serv => serv.AddNewsToDatabase(It.IsAny<News>())).Throws(new NewsExistException(ApplicationMessages.NewsAlreadyInRecord));
            var _controller = new NewsController(MockService.Object);

            //Act
            var acctual = _controller.AddNewsToDatabase(postedObject);

            //Assert
            var model = Assert.IsType<BadRequestObjectResult>(acctual);
            Assert.Equal(HttpStatusCode.BadRequest, (HttpStatusCode)model.StatusCode);
            Assert.Equal(ApplicationMessages.NewsAlreadyInRecord, model.Value);

        }

        [Fact]
        public void AddNewsToDatabase_ShouldReturnException()
        {
            //Arrange
            News postedObject = new News
            {
                Name = "Test1",
                Title = "Test1",
                Content = "Comment 1",
                UrlToImage = "PosterPath1.jpg",
                PublishedAt = "01/02/2019",
                Url = "timesofindia.in",
                UserId = "586535"
            };
            var MockService = new Mock<INewsCruiserService>();
            MockService.Setup(serv => serv.AddNewsToDatabase(It.IsAny<News>())).Throws(new Exception(ApplicationMessages.CustomExceptionMsg));
            var _controller = new NewsController(MockService.Object);

            //Act
            var acctual = _controller.AddNewsToDatabase(postedObject);

            //Assert
            var model = Assert.IsType<BadRequestObjectResult>(acctual);
            Assert.Equal(HttpStatusCode.BadRequest, (HttpStatusCode)model.StatusCode);
            Assert.Equal(ApplicationMessages.CustomExceptionMsg, model.Value);
        }

        [Fact]
        public void AddNewsToDatabase_IfModelNotValid()
        {
            //Arrange
            News postedObject = new News
            {
                Name = "",
                Title = "",
                Content = "Comment 1",
                UrlToImage = "PosterPath1.jpg",
                PublishedAt = "01/02/2019",
                Url = "timesofindia.in",
                UserId = "586535"
            };
            var MockService = new Mock<INewsCruiserService>();
           
            var _controller = new NewsController(MockService.Object);
            _controller.ModelState.AddModelError("name", ApplicationMessages.NotAValidModelMsg);

            // act
            // Now call the controller action and it will 
            // enter the (!ModelState.IsValid) condition
            var acctual = _controller.AddNewsToDatabase(postedObject);
            //Assert
            var model = Assert.IsType<BadRequestObjectResult>(acctual);
            Assert.Equal(HttpStatusCode.BadRequest, (HttpStatusCode)model.StatusCode);
            Assert.Equal(ApplicationMessages.NotAValidModelMsg, model.Value);

        }
        #endregion

        

        #region DaleteNewsFromDatabase
        [Fact]
        public void DaleteNewsFromDatabase_ShouldReturnSuccessMessage()
        {
            //Arrange
            var MockService = new Mock<INewsCruiserService>();
            MockService.Setup(serv => serv.DaleteNewsFromDatabase(It.IsAny<int>(), It.IsAny<string>())).Returns("Success");
            var _controller = new NewsController(MockService.Object);

            //Act
            var acctual = _controller.DaleteNewsFromDatabase(1122,"586535");

            //Assert
            var model = Assert.IsType<NoContentResult>(acctual);
            Assert.Equal(HttpStatusCode.NoContent, (HttpStatusCode)model.StatusCode);
            //Assert.Equal(ApplicationMessages.NewsAlreadyInRecord, model.Value);
        }

        [Fact]
        public void DaleteNewsFromDatabase_ShouldReturnNewsNotFoundException()
        {
            //Arrange
            var MockService = new Mock<INewsCruiserService>();
            MockService.Setup(serv => serv.DaleteNewsFromDatabase(It.IsAny<int>(), It.IsAny<string>())).Throws(new NewsNotFoundException(ApplicationMessages.NewsNotFoundMsg));
            
            var _controller = new NewsController(MockService.Object);
            //Act
            var acctual = _controller.DaleteNewsFromDatabase(1129,"");
            //Assert
            var model = Assert.IsType<NotFoundObjectResult>(acctual);
            Assert.Equal(HttpStatusCode.NotFound, (HttpStatusCode)model.StatusCode);
            Assert.Equal(ApplicationMessages.NewsNotFoundMsg, model.Value);
        }

      

        #endregion
        

    }
}
