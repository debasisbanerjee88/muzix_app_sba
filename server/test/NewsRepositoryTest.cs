﻿using ExpectedObjects;
using server.Data.Model;
using server.Data.Persistance;
using System.Collections.Generic;
using Xunit;

namespace test
{
    public class NewsRepositoryTest: IClassFixture<DatabaseFixture>
    {
        private readonly INewsRepository _repo;
        DatabaseFixture _databaseFixture;
        
        public NewsRepositoryTest(DatabaseFixture databaseFixture)
        {
            _databaseFixture = databaseFixture;
            _repo = new NewsRepository(_databaseFixture.NewsDbContext);
        }
        #region GetAllNews
       
        /// <summary>
        /// Test to return all News 
        /// </summary>
        [Fact]
        public void GetAllNews_ShouldReturnAllNews()
        {
            //Act
            var acctual = _repo.GetAllNews("586535");
            Assert.IsAssignableFrom<List<News>>(acctual);
            Assert.Equal(4, acctual.Count);
        }
        #endregion

        #region GetNewsById
        [Fact]
        public void GetNewsById_ShouldReturnANewsMatchedWithId()
        {
            //Arrange
            var expectedReturn = new News
            {
                NewsId = 1122,
                Name = "Test1",
                Title = "Test1",
                Content = "Comment 1",
                UrlToImage = "PosterPath1.jpg",
                PublishedAt = "01/02/2019",
                Url = "timesofindia.in",
                UserId = "586535"
            }.ToExpectedObject();
            
            //Act
            var acctual = _repo.GetNewsById(1122,"586535");

            //Assert
            Assert.IsAssignableFrom<News>(acctual);
            expectedReturn.ShouldEqual(acctual);
        }
        #endregion

        #region AddNewsToDatabase
        [Fact]
        public void AddNewsToDatabase_ShouldReturnSuccessMessage()
        {

            News postedObject = new News
            {
                Name = "Test1",
                Content = "Comment 1",
                UrlToImage = "PosterPath1.jpg",
                PublishedAt = "01/02/2019",
                Url = "timesofindia.in",
                UserId = "586535"
            };
            //Act
            var acctual = _repo.AddNewsToDatabase(postedObject);

            //Assert
            Assert.Equal("Success", acctual);

        }

        [Fact]
        public void AddNewsToDatabase_ShouldReturnException()
        {
            //act and assert
            Assert.Throws<System.ArgumentNullException>(()=> _repo.AddNewsToDatabase(null));
        }
        #endregion

        #region DaleteNewsFromDatabase
        [Fact]
        public void DaleteNewsFromDatabase_ShouldReturnSuccessMessage()
        {
            //Arrange
            int Id = 1122;
            string userId = "586535";
            //Act
            var result = _repo.DaleteNewsFromDatabase(Id,userId);

            //Assert
            Assert.Equal("Success", result);
        }
        
        [Fact]
        public void DaleteNewsFromDatabase_ShouldReturnException()
        {
            
            //act and assert
            Assert.Throws<System.ArgumentNullException>(() => _repo.DaleteNewsFromDatabase(1,""));
        }
        
        #endregion
        
    }
}
