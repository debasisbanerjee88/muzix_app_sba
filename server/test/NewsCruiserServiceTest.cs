﻿using ExpectedObjects;
using Moq;
using server.CustomException;
using server.Data.Model;
using server.Data.Persistance;
using server.MessageConstants;
using server.Service;
using System;
using System.Collections.Generic;
using Xunit;

namespace test
{
    public class NewsCruiserServiceTest 
    {
        
        private List<News> GetAllNews()
        {
            List<News> News = new List<News>();
            News.Add(new News { NewsId = 1122, Name = "Test1", Title = "Title1", Content = "Comment 1", UrlToImage = "PosterPath1.jpg", PublishedAt = "01/02/2019", Url = "timesofindia.in", UserId = "586535" });
            News.Add(new News { NewsId = 1123, Name = "Test2", Title = "Title2", Content = "Comment 2", UrlToImage = "PosterPath2.jpg", PublishedAt = "02/02/2019", Url = "timesofindia.in", UserId = "586535" });
            News.Add(new News { NewsId = 1124, Name = "Test3", Title = "Title3", Content = "Comment 3", UrlToImage = "PosterPath3.jpg", PublishedAt = "03/02/2019", Url = "timesofindia.in", UserId = "586535" });
            News.Add(new News { NewsId = 1125, Name = "Test4", Title = "Title4", Content = "Comment 4", UrlToImage = "PosterPath4.jpg", PublishedAt = "04/02/2019", Url = "timesofindia.in", UserId = "586535" });
            return News;
        }

        #region GetAllNews
        /// <summary>
        /// Test to return all News 
        /// </summary>
        [Fact]
        public void GetAllNews_ShouldReturnAllNews()
        {
            //Arrange
            var MockRepo = new Mock<INewsRepository>();
            MockRepo.Setup(repo => repo.GetAllNews("586535")).Returns(this.GetAllNews());
            var _service = new NewsCruiserService(MockRepo.Object);
            //Act
            var acctual = _service.GetAllNews("586535");
            
            //Assert
            Assert.IsAssignableFrom<List<News>>(acctual);
            Assert.NotNull(acctual);
            Assert.Equal(4, acctual.Count);
        }
        #endregion

        #region GetNewsById
        [Fact]
        public void GetNewsById_ShouldReturnANews()
        {
            //Arrange
            var expectedReturn = new News
            {
                NewsId = 1122,
                Name = "Test1",
                Title = "Test1",
                Content = "Comment 1",
                UrlToImage = "PosterPath1.jpg",
                PublishedAt = "01/02/2019",
                Url = "timesofindia.in",
                UserId = "586535"
            };
            var MockRepo = new Mock<INewsRepository>();
            MockRepo.Setup(repo => repo.GetNewsById(1122,"586535")).Returns(expectedReturn);
            var _service = new NewsCruiserService(MockRepo.Object);

            

            //Act
            var acctual = _service.GetNewsById(1122,"586535");

            //Assert
            Assert.IsAssignableFrom<News>(acctual);
            expectedReturn.ToExpectedObject().ShouldEqual(acctual);
        }

        [Fact]
        public void GetNewsById_ShouldReturnNewsNotFoundException()
        {
            //Arrange
            var MockRepo = new Mock<INewsRepository>();
            MockRepo.Setup(repo => repo.GetNewsById(1129,"")).Throws(new NewsNotFoundException("No such News found!"));
            var _service = new NewsCruiserService(MockRepo.Object);

            //Act
            NewsNotFoundException ex = Assert.Throws<NewsNotFoundException>(() => _service.GetNewsById(1129,""));

            //Assert
            Assert.Throws<NewsNotFoundException>(() => _service.GetNewsById(1129,""));
            Assert.Equal("No such News found!", ex.Message);

        }

        [Fact]
        public void GetNewsById_ShouldReturnANewsIdWithZero()
        {
            //Arrange
            var MockRepo = new Mock<INewsRepository>();
            MockRepo.Setup(repo => repo.GetNewsById(0,"")).Throws(new NewsNotFoundException(ApplicationMessages.NewsNotFoundMsg));
            var _service = new NewsCruiserService(MockRepo.Object);
            //ACT
            NewsNotFoundException ex = Assert.Throws<NewsNotFoundException>(() => _service.GetNewsById(0,""));
            //Assert
            Assert.Throws<NewsNotFoundException>(() => _service.GetNewsById(0,""));
            Assert.Equal("No such News found!", ex.Message);
        }
        #endregion
        #region GetNewsByTitle
        [Fact]
        public void GetNewsByTitle_ShouldReturnANews()
        {
            //Arrange
            var expectedReturn = new News
            {
                NewsId = 1122,
                Name = "Test1",
                Title = "Test1",
                Content = "Comment 1",
                UrlToImage = "PosterPath1.jpg",
                PublishedAt = "01/02/2019",
                Url = "timesofindia.in",
                UserId = "586535"
            };
            var MockRepo = new Mock<INewsRepository>();
            MockRepo.Setup(repo => repo.GetNewsByTitle("Test1", "586535")).Returns(expectedReturn);
            var _service = new NewsCruiserService(MockRepo.Object);



            //Act
            var acctual = _service.GetNewsByTitle("Test1", "586535");

            //Assert
            Assert.IsAssignableFrom<News>(acctual);
            expectedReturn.ToExpectedObject().ShouldEqual(acctual);
        }

        [Fact]
        public void GetNewsByTitle_ShouldReturnNewsNotFoundException()
        {
            //Arrange
            var MockRepo = new Mock<INewsRepository>();
            MockRepo.Setup(repo => repo.GetNewsByTitle("No News", "")).Throws(new NewsNotFoundException("No such News found!"));
            var _service = new NewsCruiserService(MockRepo.Object);

            //Act
            NewsNotFoundException ex = Assert.Throws<NewsNotFoundException>(() => _service.GetNewsByTitle("No News", ""));

            //Assert
            Assert.Throws<NewsNotFoundException>(() => _service.GetNewsByTitle("No News", ""));
            Assert.Equal("No such News found!", ex.Message);

        }

        [Fact]
        public void GetNewsByTitle_ShouldReturnANewsTitleWithEmpty()
        {
            //Arrange
            var MockRepo = new Mock<INewsRepository>();
            MockRepo.Setup(repo => repo.GetNewsByTitle("","")).Throws(new NewsNotFoundException(ApplicationMessages.NewsNotFoundMsg));
            var _service = new NewsCruiserService(MockRepo.Object);
            //ACT
            NewsNotFoundException ex = Assert.Throws<NewsNotFoundException>(() => _service.GetNewsByTitle("", ""));
            //Assert
            Assert.Throws<NewsNotFoundException>(() => _service.GetNewsByTitle("", ""));
            Assert.Equal("No such News found!", ex.Message);
        }
        #endregion

        #region AddNewsToDatabase
        [Fact]
        public void AddNewsToDatabase_ShouldReturnSuccessMessage()
        {
            //Arrange
            News postedObject = new News
            {
                Name = "Test1",
                Title = "Test1",
                Content = "Comment 1",
                UrlToImage = "PosterPath1.jpg",
                PublishedAt = "01/02/2019",
                Url = "timesofindia.in",
                UserId = "586535"
            };
            
            var MockRepo = new Mock<INewsRepository>();
            MockRepo.Setup(repo => repo.AddNewsToDatabase(It.IsAny<News>())).Returns("Success");
            var _service = new NewsCruiserService(MockRepo.Object);
            //Act
            var acctual = _service.AddNewsToDatabase(postedObject);

            //Assert
            Assert.Equal("Success", acctual);

        }

        [Fact]
        public void AddNewsToDatabase_ShouldReturnExceptionMessage()
        {
            //Arrange
            News postedObject = new News
            {
                Name = "Test1",
                Title = "Test1",
                Content = "Comment 1",
                UrlToImage = "PosterPath1.jpg",
                PublishedAt = "01/02/2019",
                Url = "timesofindia.in",
                UserId = "586535"
            };
            var MockRepo = new Mock<INewsRepository>();
            MockRepo.Setup(repo => repo.GetNewsByTitle(postedObject.Title,postedObject.UserId)).Throws(new NewsExistException(ApplicationMessages.NewsAlreadyInRecord));
            var _service = new NewsCruiserService(MockRepo.Object);

            //ACT
            NewsExistException ex = Assert.Throws<NewsExistException>(() => _service.AddNewsToDatabase(postedObject));

            //Assert
            Assert.Equal(ApplicationMessages.NewsAlreadyInRecord, ex.Message);

        }

        [Fact]
        public void AddNewsToDatabase_ShouldReturnSystemException()
        {
            var MockRepo = new Mock<INewsRepository>();
            MockRepo.Setup(repo => repo.AddNewsToDatabase(It.IsAny<News>())).Throws(new System.NullReferenceException());
            var _service = new NewsCruiserService(MockRepo.Object);

            //Act and Assert
            Assert.Throws<System.NullReferenceException>(() => _service.AddNewsToDatabase(null));
        }
        #endregion

        #region DaleteNewsFromDatabase
        [Fact]
        public void DaleteNewsFromDatabase_ShouldReturnSuccessMessage()
        {
            //Arrange
            var MockRepo = new Mock<INewsRepository>();
            MockRepo.Setup(repo => repo.DaleteNewsFromDatabase(1122,"586535")).Returns("Success");
            MockRepo.Setup(repo => repo.GetNewsById(1122,"586535")).Returns(this.GetAllNews().Find(m=>(m.NewsId==1122 && m.UserId=="586535")));
            var _service = new NewsCruiserService(MockRepo.Object);

            //Act
            var result = _service.DaleteNewsFromDatabase(1122,"586535");

            //Assert
            Assert.Equal("Success", result);
        }

        [Fact]
        public void DaleteNewsFromDatabase_ShouldReturnNewsNotFoundException()
        {
            //Arrange
            var MockRepo = new Mock<INewsRepository>();
            MockRepo.Setup(repo => repo.DaleteNewsFromDatabase(It.IsAny<int>(), It.IsAny<string>())).Throws(new NewsNotFoundException(ApplicationMessages.NewsNotFoundMsg));
            var _service = new NewsCruiserService(MockRepo.Object);
            //Act
            NewsNotFoundException Exception = Assert.Throws<NewsNotFoundException>(() => _service.DaleteNewsFromDatabase(1129,""));

            //Assert
            Assert.Equal(ApplicationMessages.NewsNotFoundMsg, Exception.Message);
        }

        [Fact]
        public void DaleteNewsFromDatabase_ShouldReturnSystemException()
        {
            //Act
            var MockRepo = new Mock<INewsRepository>();
            MockRepo.Setup(repo => repo.GetNewsById(It.IsAny<int>(), It.IsAny<string>())).Returns(this.GetAllNews().Find(m => (m.NewsId == 1122 && m.UserId == "586535")));
            MockRepo.Setup(repo => repo.DaleteNewsFromDatabase(It.IsAny<int>(), It.IsAny<string>())).Throws(new NullReferenceException());
            var _service = new NewsCruiserService(MockRepo.Object);

            //act and assert
            Assert.Throws<NullReferenceException>(() => _service.DaleteNewsFromDatabase(1,""));
        }


        #endregion
        

    }
}
