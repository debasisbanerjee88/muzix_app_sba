﻿using Microsoft.EntityFrameworkCore;
using server.Data.Model;
using server.Data.Persistance;
using System;
using System.Collections.Generic;
namespace test
{
    public class DatabaseFixture : IDisposable
    {
        public List<News> News { get; set; }
        public NewsDbContext NewsDbContext;
        public DatabaseFixture()
        {
            var option = new DbContextOptionsBuilder<NewsDbContext>()
                .UseInMemoryDatabase(databaseName: "NewsTestDB").Options;

            NewsDbContext = new NewsDbContext(option);

            NewsDbContext.News.Add(new News { NewsId = 1122, Name = "Test1", Title= "Test1", Content = "Comment 1", UrlToImage = "PosterPath1.jpg", PublishedAt = "01/02/2019", Url = "timesofindia.in", UserId="586535" });
            NewsDbContext.News.Add(new News { NewsId = 1123, Name = "Test2", Title= "Test2", Content = "Comment 2", UrlToImage = "PosterPath2.jpg", PublishedAt = "02/02/2019", Url = "timesofindia.in", UserId="586535" });
            NewsDbContext.News.Add(new News { NewsId = 1124, Name = "Test3", Title= "Test3", Content = "Comment 3", UrlToImage = "PosterPath3.jpg", PublishedAt = "03/02/2019", Url = "timesofindia.in", UserId="586535" });
            NewsDbContext.News.Add(new News { NewsId = 1125, Name = "Test4", Title= "Test4", Content = "Comment 4", UrlToImage = "PosterPath4.jpg", PublishedAt = "04/02/2019", Url = "timesofindia.in", UserId="586535" });

            NewsDbContext.SaveChanges();
        }
        public void Dispose()
        {
            News = null;
            NewsDbContext = null;
        }
    }
}
