﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using server.ApplicationConstants;
using server.CustomException;
using server.Data.Model;
using server.MessageConstants;
using server.Service;
using System;
using System.Net;

namespace server.Controllers
{
    //[Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class NewsController : ControllerBase
    {
        public INewsCruiserService _NewsService ;
        
        public NewsController(INewsCruiserService newsService)
        {
            this._NewsService = newsService;
        }

        

        #region GetAllNews
        /// <summary>
        /// Retrieve all News which are already in database table Favourite
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetAllNews/{userId}")]
        public IActionResult GetAllNews(string userId)
        {
            try
            {
                var newsList = _NewsService.GetAllNews(userId);
                return Ok(newsList);
            }
            catch (Exception) {
                return BadRequest(ApplicationMessages.CustomExceptionMsg);
            }
        }
        #endregion

        #region GetNewsById
        /// <summary>
        /// Retrieve News by News id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet("GetNewsById/{Id}/{userId}")]
        public IActionResult GetNewsById(string Id, string userId)
        {
            try
            {
                //Retrieve News by News id
                var news = _NewsService.GetNewsById(Id,userId);
                return Ok(news);
                
            }
            catch (NewsNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception)
            {
                return BadRequest(ApplicationMessages.CustomExceptionMsg);
            }
        }
        #endregion

        #region AddNewsToDatabase
        /// <summary>
        /// Saveing News into watch list
        /// </summary>
        /// <param name="News"></param>
        /// <returns></returns>
        [HttpPost("AddNewsToDatabase")]
        public IActionResult AddNewsToDatabase([FromBody]Muzixs news)
        {
            //checking model state
            if (!ModelState.IsValid)
            { 
                return BadRequest(ApplicationMessages.NotAValidModelMsg);
            }
            try
            {
                string message = _NewsService.AddNewsToDatabase(news);
                return Ok();
            }
            catch (NewsExistException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception)
            {
                return BadRequest(ApplicationMessages.CustomExceptionMsg);
            }

        }
        #endregion

        #region DaleteNewsFromDatabase
        /// <summary>
        /// Delete from table based on News id and News name
        /// </summary>
        /// <param name="id"></param>
        /// <param name="NewsName"></param>
        /// <returns></returns>
        [HttpDelete("DaleteNewsFromDatabase/{Id}/{userId}")]
        public IActionResult DaleteNewsFromDatabase(string Id, string userId)
        {
            try
            {
                _NewsService.DaleteNewsFromDatabase(Id,userId);
                return NoContent();
            }
            catch (NewsNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception)
            {
                return BadRequest(ApplicationMessages.CustomExceptionMsg);
            }
        }
        #endregion
    }
}
