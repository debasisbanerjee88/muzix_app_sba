﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace server.ApplicationConstants
{
    public static class NewsApiUrlConstants
    {
        public static string API_BASE_TOP_HEADLINES = "https://newsapi.org/v2/top-headlines?";
        public static string API_BASE_EVERYTHING = "https://newsapi.org/v2/everything?";
        public static string DEFAULT_COUNTRY = "country=in";
        public static string API_KEY = "apiKey=8d8f06ac782d4716835b78ef26a4021f";
        public static string DEFAULT_SORT = "sortBy=popularity&";
        public static string CATEGORY = "category=";
        public static string PAGE = "page=1";
        public static string LANGUAGE = "language=en";
    }
}
