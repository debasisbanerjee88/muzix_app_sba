﻿using server.CustomException;
using server.Data.Model;
using server.Data.Persistance;
using server.MessageConstants;
using System;
using System.Collections.Generic;

namespace server.Service
{
    public class NewsCruiserService : INewsCruiserService
    {
        private readonly INewsRepository _NewsRepo;
        public NewsCruiserService(INewsRepository NewsRepo)
        {
            _NewsRepo = NewsRepo;
        }

        #region GetAllNews
        /// <summary>
        /// Retrieve all News 
        /// </summary>
        /// <returns></returns>
        public List<Muzixs> GetAllNews(string userId)
        {
            try
            {
                List<Muzixs> News = _NewsRepo.GetAllNews(userId);
                return News;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

      

        #region GetNewsById
        /// <summary>
        /// Retrieve  News  by id 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public Muzixs GetNewsById(string Id, string userId)
        {
            try
            {
                var News = _NewsRepo.GetNewsById(Id,userId);
                if (News != null)
                    return News;
                else
                    //If News not in database then throw NewsNotFound Exception with custom exception message
                    throw new NewsNotFoundException(ApplicationMessages.NewsNotFoundMsg);
            }
            catch (NewsNotFoundException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region AddNewsToDatabase
        /// <summary>
        /// Add News to database
        /// </summary>
        /// <param name="News"></param>
        /// <returns></returns>
        public string AddNewsToDatabase(Muzixs News)
        {
            try
            {
                //check if News already in database
                if (_NewsRepo.GetNewsById(News.Id,News.UserId) == null)
                {
                    string message = _NewsRepo.AddNewsToDatabase(News);
                    return message;
                }
                else
                {
                    //thow NewsExsistException if News already in database
                    throw new NewsExistException(ApplicationMessages.NewsAlreadyInRecord);
                }
            }
            catch (NewsExistException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        

        #region DaleteNewsFromDatabase
        /// <summary>
        /// Delete News from database by Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public string DaleteNewsFromDatabase(string Id, string userId)
        {

            try
            {
                //check if News already in database
                var News = _NewsRepo.GetNewsById(Id,userId);
                if (News != null)
                {
                    string message = _NewsRepo.DaleteNewsFromDatabase(News.Id,News.UserId);
                    return message;
                }
                else
                {
                    //If News not in database then throw NewsNotFound Exception with custom exception message
                    throw new NewsNotFoundException(ApplicationMessages.NewsNotFoundMsg);
                }
            }
            catch (NewsNotFoundException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion
    }
}
