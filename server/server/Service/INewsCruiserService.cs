﻿using server.Data.Model;
using System.Collections.Generic;

namespace server.Service
{
    public interface INewsCruiserService
    {
        List<Muzixs> GetAllNews(string userId);

        Muzixs GetNewsById(string Id, string userId);

        string AddNewsToDatabase(Muzixs News);

        string DaleteNewsFromDatabase(string Id, string userId);
    }
}
