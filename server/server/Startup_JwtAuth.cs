﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Text;

namespace server
{
    public partial class Startup
    {
        public void ConfigurJwtAuth(IConfiguration configuration, IServiceCollection services)
        {
            var audianceConfig = configuration.GetSection("Audience");
            var symmetricKeyAsBase64 = audianceConfig["Secret"];
            var keyByteArray = Encoding.ASCII.GetBytes(symmetricKeyAsBase64);
            var signingKey = new SymmetricSecurityKey(keyByteArray);

            var tokenValidationParam = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = signingKey,

                ValidateIssuer = true,
                ValidIssuer = audianceConfig["Iss"],

                ValidateAudience = true,
                ValidAudience = audianceConfig["Aud"],

                ValidateLifetime = true,

                ClockSkew = TimeSpan.Zero
            };
            services.AddAuthentication(options => {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(opt=> 
            {
                opt.TokenValidationParameters = tokenValidationParam;
            });
        }
    }
}
