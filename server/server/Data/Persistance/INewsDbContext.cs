﻿using Microsoft.EntityFrameworkCore;
using server.Data.Model;
using System.Collections.Generic;

namespace server.Data.Persistance
{
    public interface INewsDbContext
    {

        DbSet<Muzixs> News { get; set; }
        int SaveChanges();
    }
}
