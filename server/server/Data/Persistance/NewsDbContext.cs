﻿using Microsoft.EntityFrameworkCore;
using server.Data.Model;

namespace server.Data.Persistance
{
    public class NewsDbContext:DbContext, INewsDbContext
    {
        public NewsDbContext() {
        }

        public NewsDbContext(DbContextOptions Options):base(Options)
        {
            this.Database.EnsureCreated();
        }
        /// <summary>
        /// Establishing composite key 
        /// </summary>
        /// <param name="builder"></param>
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Muzixs>().HasKey(table => new {
                table.Id,
                table.UserId
            });
        }

        public DbSet<Muzixs> News { get; set; }
    }
}
