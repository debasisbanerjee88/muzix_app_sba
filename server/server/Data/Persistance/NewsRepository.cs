﻿using server.Data.Model;
using server.MessageConstants;
using System;
using System.Collections.Generic;
using System.Linq;

namespace server.Data.Persistance
{
    public class NewsRepository : INewsRepository
    {
        private readonly NewsDbContext _dbcontext;
        public NewsRepository(NewsDbContext dbcontext)
        {
            _dbcontext = dbcontext;
        }

        #region GetAllNews
        /// <summary>
        /// Retrive all News 
        /// </summary>
        /// <returns></returns>
        public List<Muzixs> GetAllNews(string userId)
        {
            return _dbcontext.News.Where(n=>n.UserId==userId).ToList();
        }
        #endregion

        #region GetNewsByTitle
      /// <summary>
      /// 
      /// </summary>
      /// <param name="Id"></param>
      /// <param name="userId"></param>
      /// <returns></returns>
        public Muzixs GetNewsById(string Id,string userId)
        {
            try
            {
                var news = _dbcontext.News.FirstOrDefault(m => (m.Id == Id && m.UserId == userId));
                return news;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region AddNewsToDatabase
        /// <summary>
        /// This method is adding a News to the database
        /// </summary>
        /// <param name="News"></param>
        /// <returns></returns>
        public string AddNewsToDatabase(Muzixs News)
        {
            try
            {
                _dbcontext.News.Add(News);
                _dbcontext.SaveChanges();
                return ApplicationMessages.SuccessMsg;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        

        #region DaleteNewsFromDatabase
        /// <summary>
        /// This method will delete News from database
        /// </summary>
        /// <param name="News"></param>
        /// <returns></returns>
        public string DaleteNewsFromDatabase(string Id,string userId)
        {
            try
            {
                var xNews = GetNewsById(Id,userId);
                _dbcontext.News.Remove(xNews);
                _dbcontext.SaveChanges();

                return ApplicationMessages.SuccessMsg;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion 
        
    }

}

