﻿using server.Data.Model;
using System.Collections.Generic;

namespace server.Data.Persistance
{
    public interface INewsRepository
    {
        List<Muzixs> GetAllNews(string userId);
        
        Muzixs GetNewsById(string Id, string userId);

        string AddNewsToDatabase(Muzixs News);

        string DaleteNewsFromDatabase(string Id, string userId);
    }
}
