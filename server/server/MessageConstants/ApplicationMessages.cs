﻿namespace server.MessageConstants
{
    public static class ApplicationMessages
    {
        public const string CustomExceptionMsg = "Error occured! Please Contact with Administrator.";
        public const string NewsNotFoundMsg = "No such News found!";
        public const string NewsAlreadyInRecord = "News already in fevourite list";
        public const string NoRecordsFoundMsg = "No record to display";
        public const string NotAValidModelMsg = "Entered Data is not valid. Please check!";
        public const string SuccessMsg = "Success";
        
    }
}
