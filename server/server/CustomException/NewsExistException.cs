﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace server.CustomException
{
    public class NewsExistException : Exception
    {
        public NewsExistException(string message) : base(message)
        {

        }
    }
}
