﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace server.CustomException
{
    public class NewsNotFoundException : Exception
    {
        public NewsNotFoundException(string message) : base(message) {
            
        }
    }
}
